************************* Description of the data in this directory *************************
____ 
.txt files:
HPb_REDOR.txt
	1H{207Pb} redor data for methylated ligand extracted for various proton groups. Measured with 20kHz MAS.
Pb-redor.txt
	1H{207Pb} redor data for non-methylated ligand extracted for various proton groups. Measured with 20kHz MAS.
quaternaryNP.txt
	31P{207Pb} redor data for methylated ligand extracted for various proton groups. Measured with 11.5kHz MAS.
primaryDNP.txt
	31P{207Pb} redor data for non-methylated ligand extracted for various proton groups. Measured with 11.5kHz MAS.

Comment: Extracting the desired data from the respective txt files directly into python can be inconvenient. pandas.read_csv works better than numpy.loadtxt, but the easiest way would be to open the .txt files in excel and copy the data of interest.
_____
folders:
the folders each contain 10 .xyz files corresponding to snapshots from MD simulations data classify as binding modes 1, 2, 2' and 3. The coordinates in the xyz files are in 1e-10 meter (1 angstrom).


************************* Adapted read_and_simulate functions *************************

# for 31P{207Pb} experiments:
def read_and_simulate(filename):
    
    # import the data from the xyz file
    sym, xyz=read_xyz_file(filename)
    xyz*= 1e-10

    # lets take the P atom, which corresponds to S spin
    xyz_P = np.asarray([xyz[wi] for wi,w in enumerate(sym) if w.startswith("P0")])

    # lets get the lead atoms that are within the cutoff radius of the phosphorus
    xyz_Pb_all = xyz[np.argwhere(sym == "PB")[:,0]]
    below_co = np.zeros(xyz_Pb_all.shape[0])
    for xyz_p in xyz_P:
        dist_hPb = [np.linalg.norm(xyz_p - xyz_pb) for xyz_pb in xyz_Pb_all]
        for ii,dd in enumerate(dist_hPb): 
            if dd <= cutoff: below_co[ii]=1
    xyz_Pb = xyz_Pb_all[np.argwhere(below_co)[:,0],:]

    # now we have the positions of the phosphorus and of the lead atoms, so we can calculate the curves for this file
    redor, N_rot = heteronuclear_only(xyz_P, xyz_Pb, n_S=xyz_P.shape[0], n_I=xyz_Pb.shape[0], N_limits=(N_rot_start,N_rot_end), T_rot=T_rot, S=S_isotope, I=I_isotope, N_samples = 10**(N_samples), p_min=0.005)
    
    return redor

# for 1H{207Pb} for non-methylated ligand
def read_and_simulate(filename):
    
    # import the data from the xyz file
    sym, xyz=read_xyz_file(filename)
    xyz*= 1e-10
    
    # now lets get the correct carbons
    xyz_C = np.asarray([xyz[wi] for wi,w in enumerate(sym) if w in ["C05", "C01", "C0F"]])

    # for each we look for the 2 closest protons, which are the S spins
    xyz_H_all = np.asarray([xyz[wi] for wi,w in enumerate(sym) if w.startswith("H")])
    xyz_H = []
    for xyz_c in xyz_C:
        dist_CH = [np.linalg.norm(xyz_h - xyz_c) for xyz_h in xyz_H_all]
        xyz_H.extend(xyz_H_all[np.argsort(dist_CH)[:2]])
    xyz_H = np.asarray(xyz_H)

    # lets get the lead atoms that are within the cutoff radius of any of the 6 protons
    xyz_Pb_all = xyz[np.argwhere(sym == "PB")[:,0]]
    below_co = np.zeros(xyz_Pb_all.shape[0])
    for xyz_h in xyz_H:
        dist_hPb = [np.linalg.norm(xyz_h - xyz_pb) for xyz_pb in xyz_Pb_all]
        minimum_lead = np.argsort(dist_hPb)[:I_minimum]
        for ii in minimum_lead: below_co[ii]=1
        for ii,dd in enumerate(dist_hPb): 
            if dd <= cutoff: below_co[ii]=1
    xyz_Pb = xyz_Pb_all[np.argwhere(below_co)[:,0],:]

    # now we have the positions of the protons and of the lead atoms, so we can calculate the curves for this file
    redor, N_rot = heteronuclear_only(xyz_H, xyz_Pb, n_S=xyz_H.shape[0], n_I=xyz_Pb.shape[0], N_limits=(N_rot_start,N_rot_end), T_rot=T_rot, S=S_isotope, I=I_isotope, N_samples = 10**(N_samples), p_min=0.005)
    
    return redor