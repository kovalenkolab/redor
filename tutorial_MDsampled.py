# %% Packages
%load_ext autoreload
%autoreload 2

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colors
import os
from joblib import Parallel, delayed

# Import the functions for qd_redor
from qd_redor.multi_spin import heteronuclear_only
from qd_redor.utility import read_xyz_file, scan_dir

blue    =    "#1EAACD"
purple  =    "#A6287F" 
yellow  =    "#FAB446" 
red  =    "#C8415F" 

cmap = cmap=colors.LinearSegmentedColormap.from_list("", [blue,red])

# %%
"""
*************************
--------- INPUT----------
*************************
"""

# define ranges in which we simulate
N_rot_start = 0 # first rotor cycle (start counting at 0)
N_rot_end = 1000 # last rotor cycle (start counting at 0)
T_rot = 1/(20*1000) # duration of a rotor cycle in seconds

# define the spin-system for the simulation (check the isotopes.csv file in qd_redor)
S_isotope = "1H" 
I_isotope = "207Pb" 

# where is the data?
directory = os.path.join(os.path.dirname(__file__),"example_data\\") # directory with the file containing the redor curves and later the results
files = scan_dir(directory, "xyz")

# where is the experiment data?
filename=os.path.dirname(__file__)+"\\example_data\\HPb_REDOR.txt"

# name of group in header of the file 
name = "N(CH3)3"
cutoff = 8e-10 #curoff until where we consider I spins
N_samples = 3 # exponent for the number of samples for monte carlo integration


#%% calculate the redor curve for each of those structures and save as txt for later use

def read_and_simulate(filename):
    
    # import the data from the xyz file
    sym, xyz=read_xyz_file(filename)
    xyz*= 1e-10
    
    # now lets get those carbons which are bound to the nitrogen
    xyz_C = np.asarray([xyz[wi] for wi,w in enumerate(sym) if w in ["C0K", "C0P", "C0T"]])

    # for each we look for the three closest protons, which are the S spins
    xyz_H_all = np.asarray([xyz[wi] for wi,w in enumerate(sym) if w.startswith("H")])
    xyz_H = []
    for xyz_c in xyz_C:
        dist_CH = [np.linalg.norm(xyz_h - xyz_c) for xyz_h in xyz_H_all]
        xyz_H.extend(xyz_H_all[np.argsort(dist_CH)[:3]])
    xyz_H = np.asarray(xyz_H)

    # lets get the lead atoms that are within the cutoff radius of any of the 9 protons
    xyz_Pb_all = xyz[np.argwhere(sym == "PB")[:,0]]
    below_co = np.zeros(xyz_Pb_all.shape[0])
    for xyz_h in xyz_H:
        dist_hPb = [np.linalg.norm(xyz_h - xyz_pb) for xyz_pb in xyz_Pb_all]
        for ii,dd in enumerate(dist_hPb): 
            if dd <= cutoff: below_co[ii]=1
    xyz_Pb = xyz_Pb_all[np.argwhere(below_co)[:,0],:]

    # now we have the positions of the protons and of the lead atoms, so we can calculate the curves for this file
    redor, N_rot = heteronuclear_only(xyz_H, xyz_Pb, n_S=xyz_H.shape[0], n_I=xyz_Pb.shape[0], N_limits=(N_rot_start,N_rot_end), T_rot=T_rot, S=S_isotope, I=I_isotope, N_samples = 10**(N_samples), p_min=0.005)
    
    return redor

average = [] # here we put the averages
individual = [] # here we put the individual curves

for folder in list(files[2].keys())[:4]: # be carefull to ignore the other subfolders in this folder
    ind = Parallel(n_jobs=4)(delayed(read_and_simulate)(file) for file in files[0][folder][:])
    ind=np.asarray(ind)
    N_rot = np.linspace(N_rot_start, N_rot_end, ind.shape[1])
    individual.append(ind)
    average.append(np.mean(ind, axis=0))

# %% read data
# load REDOR curves from the experiment
data = pd.read_csv(filename, delimiter = "\t", decimal=",")
exp_1 = np.asarray(data[name])
exp = np.empty(exp_1.shape)
for i,e in enumerate(exp_1):
    if e == "--": exp[i] = None
    else: exp[i] = float(e.replace(",", "."))

N_dat = np.asarray(data["Loops"])


#%%

fig=plt.figure(1,dpi=300, figsize=(3,3))
axs = plt.axes()
alpha = [0.15, 0.15, 0.15, 0.2]
c = [blue, purple, yellow, red]
for k, ind in enumerate(individual):
    color = cmap(np.linspace(0,1,ind.shape[0]))
    for n,I in enumerate(ind):
        axs.plot(N_rot*T_rot, I, c=c[k], alpha=alpha[k]*0.7, zorder=1)
    axs.plot(N_rot*T_rot, average[k], c=c[k], label = list(files[2].keys())[k].strip("\ "), zorder=2)
    
axs.scatter(N_dat*T_rot, exp, marker="o", edgecolor="None", alpha=0.5,c="k", label="Exp", zorder=3)

axs.legend(loc='upper left')
axs.set_xlabel("Time (s)")
axs.set_ylabel("$\Delta S / S_0$")
axs.set_xlim(-20*T_rot,400*T_rot)
axs.set_ylim(-0.01,0.3)

# %%

# %%
