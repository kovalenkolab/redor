The package simulates REDOR curves for two-spin and multi-spin (excluding heteronuclear coupling) systems.
If you use this package, please ackowledge it by citing

Morad, V., Stelmakh, A., Svyrydenko, M. et al. Designer phospholipid capping ligands for soft metal halide nanocrystals. Nature 626, 542–548 (2024). https://doi.org/10.1038/s41586-023-06932-6

The subfolder REDOR includes all relevant functions that are required to simulate the curves. Please refer to the respective files for references and description. 

### Requirements
The simulations is run in Python 3.9.0  using the follwoing packages
* numpy (1.21.0), numba (0.53.1), scipy (1.6.2)
* Additionally for tutorials: pandas (1.2.4), matplotlib (3.3.4)

Note that the versions are those that we used but others might work as well. 

### Installation
To use this code, clone this repository and install the required packages using for example pip or conda. Installations will only take a few seconds to minutes. You should now be able to run the tutorial scripts.

### Tutorials 
Three tutorials with comments are provided in this directory. Sample data for comparing simulated curves with experimental curves are provided in the subfolder "example_data". In all three tutorials, we compare simulaitons to 1H{207Pb} redor data from the ammonium ligand group to lead atoms.
* The first tutorial (tutorial_twospin.py) is simulates a two_spin system with different S-I distances. The resulting distances will be an underestimation due to the presence of multiple I spins in the experiment. Running this script takes roughly 10 seconds.
* The second tutorial (tutorial_manyspins.py) simulates curves for a system of multiple I spins. We prepare a "crystal" of 207Pb atoms based on the lattice constant assuming ABr-terminated surface and put the ammonium group in the A-site of the crystal surface. We observe that this geometry fits the data well. Running this script takes <10 seconds. This script takes roughly 10-20 seconds.
* The third tutorial (tutorial_MDsampled.py) also simulates curves for a system of multiple I spins. Here the geometry is based on molecular dynamics (MD) simulations and we average the redor curves of 10 snapshots of the MD. This script takes slightly longer than the other two but still <4 minutes.

The figures that you expect are stored in the subfolder "expected_results". Note that runtime strongly depends on the parameters chosen, particularly size of the spin system (cutoff) and number of samples in the Monte Carlo integration.

### Reproduce our work
In the subfolder "example_data", you can find all data needed to reproduce our simulations. A readme.txt file is provided that describes the different files containing experimental data and snapshots from MD simulations. To generate our results, adapt tutorial_MDsampled.py to the respective experiment:
* change rotor periods, isotopes and filenames
* adapt the read_and_simulate function for the correct nuclei. Adapted functions are als found in the readme.txt file

### Apply this code to your own data
To run the code with your won data, add this repository's path to your system path by adding the following lines to your python script before importing functions from qd_redor: 
```
import sys 
sys.path.append("Put the global path of the repository here") 
```
You will also need to adapt how the data is important based on the format of your data.

