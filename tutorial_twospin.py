#%% Load packages
%load_ext autoreload
%autoreload 2

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colors

# Import the functions for qd_redor
from qd_redor.two_spin import two_spin

#%% Input
"""
*************************
--------- INPUT----------
*************************
"""

# define duration of a rotor cycle in seconds
T_rot = 1/(20*1000) 

# define the spin-system for the simulation
S_nucleus = "1H" # check the correct names in the 
I_nucleus = "207Pb"

# where is the data?
directory = "./example_data" # directory with the file containing the redor curves and later the results
filename = directory + "/HPb_REDOR.txt"  # filename of an ASCII file with redor curves

# name of group in header of the file 
name = "N(CH3)3" # check name with >>> pd.read_csv(filename, delimiter = "\t", decimal=",")
n_S=1

# define range of distances for the simulation and how many curves in this range
d_min = 3 # minimum distance in Angstrom
d_max = 4.5 # maximum distance in Angstrom
d_number = 4 # number of redor curves in this distance range

# define limits for the plotting (e.g. initial rate only)
x_limit = 500 # you can put None
y_limit = 0.4 # you can put None

# Define the colormap for plotting:
cmap=colors.LinearSegmentedColormap.from_list("", ["#648FFF","#FE6100"])
#%% Main
"""
*************************
---------- Main ---------
*************************
"""
# load REDOR curves from the experiment
data = pd.read_csv(filename, delimiter = "\t", decimal=",")
exp_1 = np.asarray(data[name])
exp = np.empty(exp_1.shape)
for i,e in enumerate(exp_1):
    if e == "--": exp[i] = None
    else: exp[i] = float(e.replace(",", "."))

N_dat = np.asarray(data["Loops"])


# now calculate the two-spin curves for 1/2 1/2 spins
REDOR, N_rot, time, distances = two_spin(N_limits=(min(N_dat),max(N_dat)), T_rot=T_rot, S=S_nucleus, I=I_nucleus, d_limits=(d_min, d_max), d_number=d_number)

# %% Plotting
"""
*************************
-------- Plotting -------
*************************
"""

# plot the data first 
fig=plt.figure(1,dpi=300, figsize=(4,4))
axs = plt.axes()

# plot experimental data points
axs.scatter(N_dat[:exp.shape[0]], exp, label=name, color="k", zorder=1, s=20)

# Plot theoretical redor curves for different distances
color = cmap(np.linspace(0,1,len(REDOR)))
for i, redor in enumerate(REDOR): 
    axs.plot(N_rot, redor, c=color[i], label=r"{} $\AA$".format(round(distances[i]*1e10, 2)), zorder=0)
axs.legend(loc="upper left")
axs.set_xlabel("$N_{rot}$")
axs.set_ylabel("$\Delta S / S_0$")
axs.set_xlim([0,x_limit])
axs.set_ylim([0, y_limit])
# %% 


