
"""
isotope information, constants and utility functions

"""
#%%
import pandas as pd
import math as m
from scipy.constants import physical_constants
import glob, os
import numpy as np

import sys
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO

from numba import jit

# useful constants in SI units:
mu_0 = physical_constants["vacuum mag. permeability"][0]
hbar = physical_constants["reduced Planck constant"][0]

def spherical_to_cartesian(r, theta, phi):
    """
    Input:
        r, theta, phi: float
            polar coordinates
    Output:
        x,y,z: float
            cartesian coordinates
    """
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    return x, y, z

def cartesian_to_spherical(x,y,z):
    """
    Input:
        x,y,z: float
            cartesian coordinates
    Output:
        r, theta, phi: float
            polar coordinates
    """
    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arctan2(np.sqrt(x**2+y**2), z)
    phi = np.arctan2( y, x )
    return r, theta, phi

# calculate dipolar coupling constants for a given distance r
def dipolar_coupling_const(gamma_S, gamma_I, r):
    """
    Input:
        gamma_S: float
            gyromagnetic ratio of S spin
        gamma_I: float
            gyromagnetic ratio of I spin
        r: float/np.1darray
            distances in meters
    Output: float/np.1darray
        dipolar coupling constant in Hz
            according to https://link.springer.com/content/pdf/10.1007/1-4020-3910-7_89.pdf
    """
    return mu_0*gamma_S*gamma_I* hbar / 2 / r**3 


def gen_lambda(N, t, D):
    """
    Input:
        N: np.1darray
            array of number of rotor cycles
        t: float
            time per roto cycle in seconds (time between two adjacent pi pulses if t/2)
        D: float
            dipolar coupling constant in Hz
    Output:¨np.1darray
        dimensionless "time" parameter 
    """
    return N * t * D

def read_xyz_file(filename, look_for_charge=False):
    """
    adapted from https://github.com/jensengroup/xyz2mol/blob/master/xyz2mol.py
    """

    atomic_symbols = []
    xyz_coordinates = []
    charge = 0
    title = ""

    with open(filename, "r") as file:
        for line_number, line in enumerate(file):
            if line_number == 0:
                num_atoms = int(line)
            elif line_number == 1:
                title = line
                if "charge=" in line:
                    charge = int(line.split("=")[1])
            else:
                atomic_symbol, x, y, z = line.split()
                atomic_symbols.append(atomic_symbol)
                xyz_coordinates.append([float(x), float(y), float(z)])
    return np.asarray(atomic_symbols), np.asarray(xyz_coordinates)

def scan_dir(path_root, extension):
    """
    scan a directory for subdirectories and files with a given extension
    arguments:
    path_root - string, path to the main directory
    extension - string
    
    returns: 
    directories - list of names of subdirectories eg. ['pos01_60nW', 'pos02_60nW'...]
    files - dictionary with keys named after subdirectories {'pos01_60nW':[list of .extension files]} 
    filepaths - dictionary with keys named after subdirectories {'pos01_60nW':[<path to a file>.extension,..]} 
    """
    os.chdir(path_root)
    directories = glob.glob("*/")
    directories.sort()
    files, filepaths = {}, {}
    for directory in directories:
        key = str(directory).strip('/')
        path = path_root + directory
        os.chdir(path)
        files[key] = glob.glob('*.{}'.format(extension))
        files[key].sort()
        filepaths[key] = []
        for file in files[key]:
            filepaths[key].append(path + file)
    return filepaths, directories, files  #directories - list, filepaths and files - dictionaries



# for the list of gyromagnetic ratios ("https://solidstatenmr.com/nuclides"):
#from_web = pd.read_html("https://solidstatenmr.com/nuclides")
#dictionary = from_web[0].set_index("Isotope").to_dict("dict")
isotopes_str = StringIO("""
Isotope,Protons,Neutrons,Symbol,Element,Nuclear Spin,Isotopic Abundance,Frequency 1H=1,Gyromagnetic Ratio (MHzT),Electric Quadrupole Moment (barn 10-28 m2)
1H,1,0,H,hydrogen,0.5,99.9885,1.0,42.576,0.0
2H,1,1,H,hydrogen,1.0,0.0115,0.153506,6.53568,0.00286
3He,2,1,He,helium,0.5,0.000137,0.761767,-32.433,0.0
6Li,3,3,Li,lithium,1.0,7.59,0.14717,6.26591,-0.000806
7Li,3,4,Li,lithium,1.5,92.41,0.388663,16.5477,-0.04
9Be,4,5,Be,beryllium,1.5,100.0,0.140529,-5.98315,0.0529
10B,5,5,B,boron,3.0,19.9,0.107456,4.57504,0.0845
11B,5,6,B,boron,1.5,80.1,0.320897,13.6625,0.04059
13C,6,7,C,carbon,0.5,1.07,0.251504,10.708,0.0
14N,7,7,N,nitrogen,1.0,99.632,0.0722848,3.0776,0.02044
15N,7,8,N,nitrogen,0.5,0.368,0.101398,-4.31712,0.0
17O,8,9,O,oxygen,2.5,0.038,0.135617,-5.77404,-0.0256
19F,9,10,F,fluorine,0.5,100.0,0.941286,40.0762,0.0
21Ne,10,11,Ne,neon,1.5,0.27,0.0789871,-3.36296,0.102
23Na,11,12,Na,sodium,1.5,100.0,0.264667,11.2685,0.104
25Mg,12,13,Mg,magnesium,2.5,10.0,0.0612601,-2.60821,0.199
27Al,13,14,Al,aluminium,2.5,100.0,0.260774,11.1027,0.1466
29Si,14,15,Si,silicon,0.5,4.6832,0.198826,-8.46521,0.0
31P,15,16,P,phosphorus,0.5,100.0,0.405178,17.2509,0.0
33S,16,17,S,sulfur,1.5,0.76,0.0768417,3.27161,-0.0678
35Cl,17,18,Cl,chlorine,1.5,75.78,0.0980928,4.1764,-0.0817
37Cl,17,20,Cl,chlorine,1.5,24.22,0.0816519,3.47641,-0.0644
39K,19,20,K,potassium,1.5,93.2581,0.0467229,1.98928,0.0585
40K,19,21,K,potassium,4.0,0.0117,0.0580993,-2.47364,-0.073
41K,19,22,K,potassium,1.5,6.7302,0.0256453,1.09187,0.0711
43Ca,20,23,Ca,calcium,3.5,0.135,0.0673811,-2.86882,-0.0408
45Sc,21,24,Sc,scandium,3.5,100.0,0.243298,10.3587,-0.22
47Ti,22,25,Ti,titanium,2.5,7.44,0.0564639,-2.40401,0.302
49Ti,22,27,Ti,titanium,3.5,5.41,0.0564795,-2.40467,0.247
50V,23,27,V,vanadium,6.0,0.25,0.0998291,4.25032,0.21
51V,23,28,V,vanadium,3.5,99.75,0.263362,11.2129,-0.043
53Cr,24,29,Cr,chromium,1.5,9.501,0.0566375,-2.4114,-0.15
55Mn,25,30,Mn,manganese,2.5,100.0,0.247292,10.5287,0.33
57Fe,26,31,Fe,iron,0.5,2.119,0.0323863,1.37888,0.0
59Co,27,32,Co,cobalt,3.5,100.0,0.236676,10.0767,0.42
61Ni,28,33,Ni,nickel,1.5,1.1399,0.0895162,-3.81124,0.162
63Cu,29,34,Cu,copper,1.5,69.17,0.265392,11.2993,-0.22
65Cu,29,36,Cu,copper,1.5,30.83,0.284262,12.1027,-0.204
67Zn,30,37,Zn,zinc,2.5,4.1,0.0626944,2.66928,0.15
69Ga,31,38,Ga,gallium,1.5,60.108,0.240684,10.2474,0.171
71Ga,31,40,Ga,gallium,1.5,39.892,0.305813,13.0203,0.107
73Ge,32,41,Ge,germanium,4.5,7.73,0.0349889,-1.48969,-0.19
75As,33,42,As,arsenic,1.5,100.0,0.171805,7.31477,0.314
77Se,34,43,Se,selenium,0.5,7.63,0.191575,8.1565,0.0
79Br,35,44,Br,bromine,1.5,50.69,0.251404,10.7038,0.313
81Br,35,46,Br,bromine,1.5,49.31,0.270997,11.538,0.262
83Kr,36,47,Kr,krypton,4.5,11.49,0.0386172,-1.64417,0.259
85Rb,37,48,Rb,rubidium,2.5,72.17,0.0968889,4.12514,0.276
87Rb,37,50,Rb,rubidium,1.5,27.83,0.328376,13.981,0.1335
87Sr,38,49,Sr,strontium,4.5,7.0,0.0434753,-1.85101,0.305
89Y,39,50,Y,yttrium,0.5,100.0,0.0492026,-2.09485,0.0
91Zr,40,51,Zr,zirconium,2.5,11.22,0.0933542,-3.97465,-0.176
93Nb,41,52,Nb,niobium,4.5,100.0,0.245484,10.4517,-0.32
95Mo,42,53,Mo,molybdenum,2.5,15.92,0.0654708,-2.78749,-0.022
97Mo,42,55,Mo,molybdenum,2.5,9.55,0.0668493,-2.84618,0.255
99Ru,44,55,Ru,ruthenium,2.5,12.76,0.0458314,-1.95132,0.079
101Ru,44,57,Ru,ruthenium,2.5,17.06,0.0515603,-2.19523,0.46
103Rh,45,58,Rh,rhodium,0.5,100.0,0.0316523,-1.34763,0.0
105Pd,46,59,Pd,palladium,2.5,22.33,0.0460104,-1.95894,0.66
107Ag,47,60,Ag,silver,0.5,51.839,0.0406646,-1.73134,0.0
109Ag,47,62,Ag,silver,0.5,48.161,0.046748,-1.99034,0.0
111Cd,48,63,Cd,cadmium,0.5,12.8,0.213003,-9.06882,0.0
113Cd,48,65,Cd,cadmium,0.5,12.22,0.22282,-9.48676,0.0
113In,49,64,In,indium,4.5,4.29,0.219955,9.36479,0.759
115In,49,66,In,indium,4.5,95.71,0.220438,9.38537,0.77
115Sn,50,65,Sn,tin,0.5,0.34,0.329001,-14.0076,0.0
117Sn,50,67,Sn,tin,0.5,7.68,0.35843,-15.2605,0.0
119Sn,50,69,Sn,tin,0.5,8.59,0.374986,-15.9654,0.0
121Sb,51,70,Sb,antimony,2.5,57.21,0.240865,10.2551,-0.543
123Sb,51,72,Sb,antimony,3.5,42.79,0.130424,5.55294,-0.692
123Te,52,71,Te,tellurium,0.5,0.89,0.26387,-11.2345,0.0
125Te,52,73,Te,tellurium,0.5,7.07,0.318136,-13.545,0.0
127I,53,74,I,iodine,2.5,100.0,0.201463,8.57748,-0.696
129Xe,54,75,Xe,xenon,0.5,26.44,0.27856,-11.86,0.0
131Xe,54,77,Xe,xenon,1.5,21.18,0.0825323,3.51389,-0.114
133Cs,55,78,Cs,caesium,3.5,100.0,0.132073,5.62316,-0.00343
135Ba,56,79,Ba,barium,1.5,6.592,0.100011,4.25806,0.16
137Ba,56,81,Ba,barium,1.5,11.232,0.111877,4.76327,0.245
138La,57,81,La,lanthanum,5.0,0.09,0.13297,5.66132,0.39
139La,57,82,La,lanthanum,3.5,99.91,0.142356,6.06094,0.2
141Pr,59,82,Pr,praesodymium,2.5,100.0,0.306175,13.0357,-0.077
143Nd,60,83,Nd,neodymium,3.5,12.2,0.0544785,-2.31947,-0.61
145Nd,60,85,Nd,neodymium,3.5,8.3,0.0334784,-1.42538,-0.314
147Sm,62,85,Sm,samarium,3.5,14.99,0.0415347,-1.76838,-0.26
149Sm,62,87,Sm,samarium,3.5,13.82,0.0341587,-1.45434,0.078
151Eu,63,88,Eu,europium,2.5,47.81,0.248617,10.5851,0.903
153Eu,63,90,Eu,europium,2.5,52.19,0.109816,4.67554,2.41
155Gd,64,91,Gd,gadolinium,1.5,14.8,0.0307034,-1.30723,1.27
157Gd,64,93,Gd,gadolinium,1.5,15.65,0.04055,-1.72646,1.35
159Tb,65,94,Tb,terbium,1.5,100.0,0.240436,10.2368,1.432
161Dy,66,95,Dy,dysprosium,2.5,18.91,0.0343735,-1.46349,2.51
163Dy,66,97,Dy,dysprosium,2.5,24.9,0.0481587,2.05041,2.65
165Ho,67,98,Ho,holmium,3.5,100.0,0.29862,12.714,3.58
167Er,68,99,Er,erbium,3.5,22.93,0.0288415,-1.22796,3.57
169Tm,69,100,Tm,thulium,0.5,100.0,0.0827113,-3.52152,0.0
171Yb,70,101,Yb,ytterbium,0.5,14.28,0.176762,7.52583,0.0
173Yb,70,103,Yb,ytterbium,2.5,16.13,0.0464043,-1.97571,2.8
175Lu,71,104,Lu,lutetium,3.5,97.41,0.114185,4.86152,3.49
176Lu,71,105,Lu,lutetium,7.0,2.59,0.0808673,3.44301,4.92
177Hf,72,105,Hf,hafnium,3.5,18.6,0.0405858,1.72798,3.37
179Hf,72,107,Hf,hafnium,4.5,13.62,0.0254937,-1.08542,3.79
181Ta,73,108,Ta,tantalum,3.5,99.988,0.121254,5.16253,3.17
183W,74,109,W,tungsten,0.5,14.31,0.0421737,1.79559,0.0
185Re,75,110,Re,rhenium,2.5,37.4,0.228226,9.71694,2.18
187Re,75,112,Re,rhenium,2.5,62.6,0.230571,9.8168,2.07
187Os,76,111,Os,osmium,0.5,1.96,0.0231491,0.985596,0.0
189Os,76,113,Os,osmium,1.5,16.15,0.0787648,3.35349,0.86
191Ir,77,114,Ir,iridium,1.5,37.3,0.0179924,0.766044,0.816
193Ir,77,116,Ir,iridium,1.5,62.7,0.019532,0.831596,0.751
195Pt,78,117,Pt,platinum,0.5,33.832,0.218236,9.29162,0.0
197Au,79,118,Au,gold,1.5,100.0,0.0173952,0.740616,0.547
199Hg,80,119,Hg,mercury,0.5,16.87,0.181136,7.71205,0.0
201Hg,80,121,Hg,mercury,1.5,13.18,0.0668644,-2.84682,0.387
203Tl,81,122,Tl,thallium,0.5,29.524,0.580862,24.7308,0.0
205Tl,81,124,Tl,thallium,0.5,70.476,0.586575,24.974,0.0
207Pb,82,125,Pb,lead,0.5,22.1,0.212171,9.03337,0.0
209Bi,83,126,Bi,bismuth,4.5,100.0,0.163525,6.96223,-0.516
235U,92,143,U,uranium,3.5,0.72,0.0195141,-0.830834,4.936
""")
isotopes = pd.read_csv(isotopes_str).set_index("Isotope")
gyro_ratio = isotopes["Gyromagnetic Ratio (MHzT)"]*1e6 # in Hz/Tesla
abundance = isotopes["Isotopic Abundance"]/100 
nuclear_spin = isotopes["Nuclear Spin"]

# %%
