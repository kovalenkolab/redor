""""
Here are some functions for tw-spin systems
Two add new ones (different spin) check 

based on table 2 in https://link.springer.com/content/pdf/10.1007%2F1-4020-3910-7_89.pdf
"""
#%%

from scipy.special import jv # besserl function of first kind
import numpy as np

from numba import jit

from .utility import gyro_ratio, abundance, dipolar_coupling_const, gen_lambda, nuclear_spin


def redor_curve(ll, S=1/2, I=1/2):
    """
    Input:
        ll: np.1darray
            dimensionless "time" parameter
    Output:¨np.1darray
        REDOR curve as np.1darray
    """
    if S == 1/2:
        if I == 1/2:
            bessel_plus = jv(1/4, np.sqrt(2)*ll)  
            bessel_minus = jv(-1/4, np.sqrt(2)*ll)
            result = np.sqrt(2)*np.pi/4 * bessel_plus * bessel_minus
        else: print("Not implemented yet")
    else: print("Not implemented yet")
    return 1-result

def two_spin(N_limits=(1,1000), T_rot=5e-5, S="1H", I="31P", distances = False, d_limits=(2,4), d_number=5):
    """
    Input:
        N_limits: tuple
            Limits for the number of rotor cycles for the simulation
        T_rot: float
            duration of a single rotor cycle
        S: string
            name of the S nucles (check names in the gyro-ratios module)
        I: string
            name of the I nucleus (check names in the gyro-ratios module)
        d_limits: tuple
            limits for the distances for which we will simulate the redor curves
        d_number: int
            number of redor curves that are generated within the distance range of
    Output
        N_rot: np.1darray
            Rotor cycle number for the simulation
        time: list of d_number np.1darray
            dimensionless time corresponding to the rotor cycle number for the different distances
        redor: list of d_number np.1darray
            redor curves corresponding to the rotor cycle number for the different distances

    """
    N_rot_start, N_rot_end = N_limits
    d_min, d_max = d_limits

    gyro_S = gyro_ratio[S] # gyromagnetic ratioof S spin in Hz/Tesla
    gyro_I = gyro_ratio[I] # gyromagnetic ratio of I spin in Hz/Tesla

    N_rot = np.linspace(N_rot_start,N_rot_end, 512) # rotor cycle numbers for defined range

    if distances == False:
        distances = np.linspace(d_min,d_max,d_number, dtype=np.float64)*1e-10 # distances in meter
    couplings = dipolar_coupling_const(gyro_S, gyro_I,distances) # get corresponding dipolar coupling
    time = [gen_lambda(N_rot,T_rot,DD) for DD in couplings] # generate the dimensionless time lambda
    redor = [redor_curve(t, S=nuclear_spin[S], I=nuclear_spin[I])*abundance[I] for t in time] # calculate redor curve
    return redor, N_rot, time, distances

# %%
