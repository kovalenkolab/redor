"""
based on proof that geometry does not matter for the initial curve:
    https://www.sciencedirect.com/science/article/pii/S0926204099000508?via%3Dihub

following:
    https://pubs.acs.org/doi/10.1021/jacs.9b13396

also important : https://reader.elsevier.com/reader/sd/pii/S1090780797911988?token=7314604B11FB7A8DA58013E82A18345FF2DC62B788A4A9621288F6525F580DC7C4DA0793FDEF1651104FA63968FE5A4A&originRegion=eu-west-1&originCreation=20211019141612
https://pdf.sciencedirectassets.com/271350/1-s2.0-S0009261400X00450/1-s2.0-000926149500773W/main.pdf?X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDgaCXVzLWVhc3QtMSJHMEUCIALO4RDUmrRGI9E7qEKn3wuaSsBxbty0s2LVLMyNkgxNAiEA9GnSRvr8wFAJ4eLlyBWMdzbLplUClEctBR1QvLPbjW4qgwQIwf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAEGgwwNTkwMDM1NDY4NjUiDJQMm61WPZIwuvxuLyrXAy1oRVG5vOMgh5aYmF1%2Fg45NaF%2FsN3j18%2BKM%2BWHk%2BUOuCdWB2O2C16mnBHj9T4djQO3VAiRZgQcmR%2BkgKGwZCPIasOsfMYDQVfWnAHYOOXfzu%2BxjsPmHV2SUzS%2Fs8DSE4RbZmU7g%2BlWV0S3hSoGMAxukuyHNQaVDuLEiMl4PvLJlO8zwg3LjXjoTgaJOkFVe%2B%2B9bYcCTaHPnqjtmC9U8gsKaIJpx8IJf8gD8YVsSE0zBUTCO31QocEvMyQAeYsXYpc%2BBrDxddg60NYuh4%2BWQD9f5R%2BGTKQ3AozEGDg3oKIqj%2FJJRwAP4Sgg3rrAL6VIyEihkbjQxjfYxyZIc5CjdgD0jHH0VxLuRYBf%2FI0BQRxkUcDN5FdJpFebaoFb%2FpWrvZGNpstcDj0JWpn%2FUN%2Bicbi2wJDN2%2BBNK8d6pD4TBJmhiIqy6EkfQUvOOB%2F1fTWfBMtqckzbp9b9PBvOXZgYyIVWm26WEoroBwEDZMkyZY7cZggRFsvKInzRsuiCosRYeQqRUAsdRVNZcd5wMiCXH5AZF2GVTDQD4d0V1iWzyQ%2FqGv%2BzFHYmmo%2F6wJVUrEafIOljuIvzOMtMFP3S1bBljBemJZ9XGkziAHS0Cfj9EyxMivwb4tob2EDDe07uLBjqlAaT7VlEaAhbSEbZ3w0lD6CHOj6PzJpP1klu1%2B49vJrPaet4DlVZOCZyqyXkTPhD3M1J2rvFiCRFor%2BpLvDFbDmye4CbkYGtMoifcUk3wVKJJXAH1BhF25MizWMd4qkTAbjIcySvmwG07MWN73c2eyRuQwwYHwedvmCHbTGeRilkN2tUZHdz%2FoDvLHkk9q9LJmTC7Cal10ssFfssulv79sq2LL3tjzQ%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20211019T163941Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAQ3PHCVTYT3RVIBJX%2F20211019%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=cd4e0beb1818f9e020893ebffdfb204f25c03a9bd6980f62f450b0be8adf20d7&hash=d6e14e6a05ebf3c19aee38676ee71c1560f3c616c28d789758ea0f9e704b639d&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=000926149500773W&tid=spdf-3aa3d1b4-1506-4d7a-94b6-edfa261d4a95&sid=750c4d00125d034cf58a1590f2fd66c6fcfdgxrqb&type=client

for now just spin 1/2
"""


#%%
import math as m
import numpy as np
from numba import vectorize, jit
from itertools import combinations
from .utility import gyro_ratio, abundance, dipolar_coupling_const, nuclear_spin, spherical_to_cartesian, cartesian_to_spherical

@vectorize
def integrand_factor(t, D, a, u, c, x, y, z):
    """
    Input: 
        t: np.1darray
            array of times
        D: float
            Dipolar coupling constant as in https://link.springer.com/content/pdf/10.1007%2F1-4020-3910-7_89.pdf
        a,u,c: float
            powder angles alpha, u=cos(beta) and gamma
        x,y,z: float
            cartesian coordinates for r=1 (https://www.rsc.org/suppdata/c9/cp/c9cp04099d/c9cp04099d1.pdf)
    Output: np.1darray
        https://www.sciencedirect.com/science/article/pii/S1090780797911988?ref=pdf_download&fr=RR-2&rr=7f89a4512bd5ce9f"""
    b = m.acos(u)
    return m.cos(    
                        ( 
                            (x*m.sin(b)*m.cos(a) + y*m.sin(b)*m.sin(a) + z*m.cos(b)) * 
                            (   
                                x*(-m.cos(c)*m.sin(a) - m.cos(b)* m.cos(a)*m.sin(c)) + 
                                y*(m.cos(c)*m.cos(a)-m.cos(b)*m.sin(a)*m.sin(c)) +
                                z*(m.sin(c)*m.sin(b))
                            ) 
                        ) * D *t * 4 * m.sqrt(2)
                    )   


@jit
def integrand(a, u, c, tau, D, x, y, z):
    """
    Input:
        a,u,c: float
            powder angles alpha, u=cos(beta) and gamma
        tau: np.1darray
            array of times
        D: np.1darrays
            Dipolar coupling constant of each spin as in https://link.springer.com/content/pdf/10.1007%2F1-4020-3910-7_89.pdf
        x,y,z: np.1darrays
            cartesian coordinates for each spin for r=1 (https://www.rsc.org/suppdata/c9/cp/c9cp04099d/c9cp04099d1.pdf)
    Ouptut: np.1darray
        integrand from eq. S1 in https://www.rsc.org/suppdata/c9/cp/c9cp04099d/c9cp04099d1.pdf for different times
    """

    # geenrate intgerand for the nspins system with constant distance
    product = np.ones(len(tau))
    for i, d in enumerate(D):
        product = product * integrand_factor(tau, d, a, u, c, x[i], y[i], z[i])
    return product

#@jit
def MC_integration(tau:np.ndarray, D, x, y, z, nspins, N_samples=10**5):
    """
    Input:
        tau: np.1darray
            array of times
        D: np.1darray
            Dipolar coupling constant of each spin as in https://link.springer.com/content/pdf/10.1007%2F1-4020-3910-7_89.pdf
        x,y,z: np.1darrays
            cartesian coordinates of each spin for r=1 (https://www.rsc.org/suppdata/c9/cp/c9cp04099d/c9cp04099d1.pdf)
        nspins: int
            number of I spins that are considered
        N_samples: int
            number of samples for the Monte Carlo integration
    Ouptut: 
        integral: np.1darray
            integral from eq. S1 in https://www.rsc.org/suppdata/c9/cp/c9cp04099d/c9cp04099d1.pdf for different times
    """
    # integration over euler angles (function is independent of gamma hence factor 2 pi)
    sumup = np.zeros(tau.shape)

    # calculate redor curves for different euler angles and average to get the powder redor curve
    for i in range(N_samples):
        # randomly draw powder angles
        alpha = np.random.uniform(0,2*m.pi)
        u = np.random.uniform(-1,1) # u = cos(b), du/db = -sin(b), b = arccos(u)
        gamma = np.random.uniform(0,2*m.pi)

        # calculate the integrand for all times tau
        sumup = sumup + integrand(alpha, u, gamma, tau, D, x, y, z)
    # some normalization is needed (no clue why sqrt of nspins) the RESPDOR curve from the paper has different ones
    integral = sumup / N_samples 
    return integral

def redor_multi(tau, D, x, y, z, nspins, S=1/2, I=1/2, N_samples=10**5):
    """
    Input:
        tau: np.1darray
            array of times
        D: np.1darray
            Dipolar coupling constants for each spin as in https://link.springer.com/content/pdf/10.1007%2F1-4020-3910-7_89.pdf
        x,y,z: np.1darrays
            cartesian coordinates of each spin for r=1 (https://www.rsc.org/suppdata/c9/cp/c9cp04099d/c9cp04099d1.pdf)
        nspins: int
            number of I spins that are considered
        N_samples: int
            number of samples for the Monte Carlo integration
        S: float (multiple of 1/2)
            nuclear spin of the S isotope
        I: float (multiple of 1/2)
            nuclear spin of the I isotope
    Ouptut: 
        Redor curve: np.1darray
            Redor curve
    """
    if S==1/2 and I==1/2:
        return 1-MC_integration(tau, D, x, y, z, nspins, N_samples=N_samples) 
    else: print("not implemented yet") # so far we only have the curve for spin 1/2 isotopes

def binomial_dist(n, k, p):
    """
    Input:
        n: int
            number of draws
        p: int
            probability of True
        k: int
            number of True draws
    Output:
        float
        binomial probability
    """
    return m.comb(n,k)*p**k*(1-p)**(n-k)

def redor_combinatorial(resolution, nspins, abundance_I, time, D, x, y, z, spin_I, spin_S, N_samples, p_min=0.000005):
    redor = np.zeros(resolution)
    for k in range(nspins+1):
        p_k = binomial_dist(nspins, k, abundance_I)
        if p_k < p_min: pass
        else:
            comb = np.asarray(list(combinations(np.arange(nspins), k)), dtype=int)
            red = np.zeros(resolution)
            for cc in comb:
                red += redor_multi(time, D[cc], x[cc], y[cc], z[cc], nspins = k, S=spin_S, I=spin_I, N_samples=int(N_samples*p_k))
            redor += red/comb.shape[0] * p_k
    return redor

def heteronuclear_only(pos_S: np.ndarray, pos_I: np.ndarray, n_S=1, n_I=1, N_limits=(1,1000), T_rot=5e-5, S="31P", I="1H", N_samples = 10**5, p_min=0.000005):
    """
    Input:
        pos_S: np.ndarray
            array with shape (n_S, 3) which contains the position of the S spin
        pos_I: np.ndarray
            array with shape (n_I, 3) which containes the positions of the I spins
        nspins: int
            number of spins
        N_limits: tuple
            limits for the rotor cycles
        T_rot: float
            duration of a rotor cycle
        S, I: string
            isotopes of S and I spins
        N_samples: int
            number of samples for the MC integration (will be scaled by the probability of each combinatoin)
    Output:
        redor: np.1darray
            redor curve for the defined system
        N_rot: np.1darray
            rotor cycles corresponding to the simulated redor curve
    """
    resolution = 512

    # rotor cycles & time
    N_rot_start, N_rot_end = N_limits
    N_rot = np.linspace(N_rot_start,N_rot_end, resolution) # rotor cycle numbers for defined range
    time = N_rot*T_rot

    # some info on the nuclei
    gyro_S = gyro_ratio[S] # gyromagnetic ratioof S spin in Hz/Tesla
    gyro_I = gyro_ratio[I] # gyromagnetic ratio of I spin in Hz/Tesla
    spin_S = nuclear_spin[S]
    spin_I = nuclear_spin[I]
    abundance_I = abundance[I]

    redor = np.zeros(resolution)
    # loop to average over nuclei that are integrated together
    for i in range(n_S):
        if n_S == 100000:    p_S = pos_S
        else: p_S = pos_S[i,:]

        print(pos_I.shape)

        
        # shift origin to pos_S and calculate polar coordinates
        pos_I_0 = pos_I - p_S

        if n_I ==10000: # this is old stuff but the data format can be the same for single spins as for multiple spins
            # coupling constants 
            r, theta, phi = cartesian_to_spherical(pos_I_0[:,0], pos_I_0[:,1], pos_I_0[:,2])
            D = np.asarray([dipolar_coupling_const(gyro_S, gyro_I,r)])

            # cartesian coordinates for r=1 which is needed for the redor curve
            x,y,z = spherical_to_cartesian(1,theta, phi)

            # just some weird syntax stuff
            x = np.asarray([x])
            y = np.asarray([y])
            z = np.asarray([z])

        else:
            # coupling constants
            r, theta, phi = cartesian_to_spherical(pos_I_0[:,0], pos_I_0[:,1], pos_I_0[:,2])
            D = np.asarray([dipolar_coupling_const(gyro_S, gyro_I,dist) for dist in r])

            # cartesian coordinates for r=1 which is needed for the redor curve
            x,y,z = spherical_to_cartesian(1,theta, phi)

        if abundance_I > 0.97:
            redor += 1/n_S*redor_multi(time, D, x,y,z,n_I, S=spin_S, I=spin_I, N_samples=N_samples)

        else:
            redor += 1/n_S*redor_combinatorial(resolution, n_I, abundance_I, time, D, x, y, z, spin_I, spin_S, N_samples, p_min=p_min)

    return redor, N_rot


