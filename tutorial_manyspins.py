"""
Here we generate the curves following the rossini approach
"""
# %% Packages
%load_ext autoreload
%autoreload 2

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colors

# Import the functions for qd_redor
from qd_redor.multi_spin import heteronuclear_only

# %%
"""
*************************
--------- INPUT----------
*************************
"""

# define ranges in which we simulate
N_rot_start = 0 # first rotor cycle (start counting at 0)
N_rot_end = 500 # last rotor cycle (start counting at 0)
T_rot = 1/(20*1000) # duration of a rotor cycle in seconds

# define the spin-system for the simulation (check the isotopes.csv file in qd_redor)
S_isotope = "1H"
I_isotope = "207Pb"

# where is the data?
directory = "./example_data" # directory with the file containing the redor curves and later the results
filename = directory + "/HPb_REDOR.txt"  # filename of an ASCII file with redor curves

# name of group in header of the file 
name = "N(CH3)3" # check name with >>> pd.read_csv(filename, delimiter = "\t", decimal=",")
n_S=9

# define limits for the plotting (e.g. initial rate only)
x_limit = N_rot_end # you can put None
y_limit = 0.4 # you can put None

# define the exponent for the number of samples for monte carlo integration
N_mc = 3 

# define cutoff (until which distance we consider I nuceli)
cutoff = 8e-10

# Define the colormap for plotting:
cmap=colors.LinearSegmentedColormap.from_list("", ["#648FFF","#FE6100"])


"""
*************************
-------- Geometry -------
*************************
"""
# here you define the geometry. In this case based on crystal structure (assuming NMe3 in lattice site) and a cutoff
positions_S = np.asarray(
    [[0,0,0]]*n_S
    )

ref_spot = positions_S[0,:]

# generate 3 unit cells in +-x, +-y, and -z
a = 5.986e-10
n_cells = 2
grid = np.array(np.meshgrid(np.arange(-n_cells, n_cells), np.arange(-n_cells, n_cells), np.arange(-n_cells, 1))).T.reshape(-1,3)
positions_I_all2 = (grid + np.array([[-1/2,-1/2,-1/2]]*grid.shape[0]))
positions_I_all = a*positions_I_all2  # get the values in 

positions_I = []
for pos in positions_I_all:
    if np.linalg.norm(ref_spot - pos) < cutoff:    positions_I.append(pos)  # get only those within the cutoff
n_I = len(positions_I) # number of I spins that need to be considered

print("number of Pb: {}".format(len(positions_I)))

positions_I = np.array(positions_I)

#%% Main
"""
*************************
---------- Main ---------
*************************
"""
# load REDOR curves from the experiment
data = pd.read_csv(filename, delimiter = "\t", decimal=",")
exp_1 = np.asarray(data[name])
exp = np.empty(exp_1.shape)
for i,e in enumerate(exp_1):
    if e == "--": exp[i] = None
    else: exp[i] = float(e.replace(",", "."))

N_dat = np.asarray(data["Loops"])

REDOR, N_rot = heteronuclear_only(positions_S, positions_I, n_S=n_S, n_I=n_I, N_limits=(N_rot_start,N_rot_end), T_rot=T_rot, S=S_isotope, I=I_isotope, N_samples = 10**N_mc, p_min=0.00005)
#%%

# plot the data first and normalize
fig=plt.figure(1,dpi=300, figsize=(4,4))
axs = plt.axes()

# plot experimental data points
color = "k"
axs.scatter(N_dat[:exp.shape[0]], exp, label=name, color=color, zorder=1, s=20)

# Plot theoretical redor curves for different distances
color = "green" #cmap(0.5)
axs.plot(N_rot, REDOR, c=color,zorder=0)
axs.legend(loc='right')
axs.set_xlabel("$N_{rot}$")
axs.set_ylabel("$\Delta S / S_0$")
axs.set_xlim([0,x_limit])

# %%
